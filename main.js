/* 1). Функции служат для решения проблемы повторяющегося кода, делают его более читаемым.
В любой момент мы можем вызвать функцию по ее имени и список команд лежащий в ней
выполнится и нам не нужно прописывать эти команды заново.
 2). При помощи аргументов мы передаем значения при вызове функции */



let num1 = +prompt("Please, enter the first number"); //первое число введенное пользователем
let num2 = +prompt("Please, enter the second number"); //второе число введенное пользователем
let operation = prompt("Please enter the operation +, -, *, /"); //выбранный оператор

function calculate(num1, num2, operation) {
    let result;
    switch (operation) {
        case "+":
            result = num1 + num2;
            break;
        case "-":
            result = num1 - num2;
            break;
        case "*":
            result = num1 * num2;
            break;
        case "/":
            result = num1 / num2;
            break;
        default:
            result = "Something went wrong";
    }
    return result;
}

console.log(calculate(num1, num2, operation));

